import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import ComingSoon from './Pages/ComingSoon'
import NotFound from './Pages/NotFound'
import AndeLumut from './Pages/AndeLumut/AndeLumut';


import AssetsLogoDjarum from './img/logo-djarum.png'

/*
https://ceritarakyatindonesia.com/lutung_kasarung
https://ceritarakyatindonesia.com/malin_kundang
https://ceritarakyatindonesia.com/ande_ande_lumut
https://ceritarakyatindonesia.com/sangi_sang_pemburu
https://ceritarakyatindonesia.com/timun_mas
https://ceritarakyatindonesia.com/keong_emas
https://ceritarakyatindonesia.com/putri_tandampalik
https://ceritarakyatindonesia.com/empat_raja
https://ceritarakyatindonesia.com/bawang_merah_bawang_putih
https://ceritarakyatindonesia.com/pesut_mahakam
https://ceritarakyatindonesia.com/sigarlaki_dan_limbat
https://ceritarakyatindonesia.com/asal_usul_danau_batur
*/

function App() {
  return (
    <div className="App h-100 overflow-hidden" style={{overflow: 'none'}}>
      <div className="position-fixed w-100 text-center p-4" id="Header">
        <img src={AssetsLogoDjarum} alt="Djarum" className="logo w-100" />
      </div>
      <Router>
        <Switch>
          <Route path="/" exact>
            <ComingSoon />
          </Route>
          <Route path="/lutung_kasarung">
            <ComingSoon />
          </Route>
          <Route path="/malin_kundang">
            <ComingSoon />
          </Route>
          <Route path="/ande_ande_lumut">
            <AndeLumut />
          </Route>
          <Route path="/sangi_sang_pemburu">
            <ComingSoon />
          </Route>
          <Route path="/timun_mas">
            <ComingSoon />
          </Route>
          <Route path="/keong_emas">
            <ComingSoon />
          </Route>
          <Route path="/putri_tandampalik">
            <ComingSoon />
          </Route>
          <Route path="/empat_raja">
            <ComingSoon />
          </Route>
          <Route path="/bawang_merah_bawang_putih">
            <ComingSoon />
          </Route>
          <Route path="/pesut_mahakam">
            <ComingSoon />
          </Route>
          <Route path="/sigarlaki_dan_limbat">
            <ComingSoon />
          </Route>
          <Route path="/asal_usul_danau_batur">
            <ComingSoon />
          </Route>
          <Route path="*">
            <NotFound />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
