import React from 'react'
import {useSpring, animated} from 'react-spring'

export default() => {
  const loading = 1;
  const FadeIn = useSpring({
    from: { opacity: 0, transform: `translate3d(0px, -20px, 0px)` },
    to : async next => {
      while(loading) {
        await next({opacity: 1, transform: `translate3d(0px, 0px, 0px)`})
        await next({ opacity: 1, transform: `translate3d(0px, -10px, 0px)`, config: { duration: 8000}})
      }
    },
    config: { duration: 1000 }
  })

  const PulseFx = useSpring({
    from: { backgroundSize: '100%'},
    to: async next => {
      while(loading) {
        await next({backgroundSize: '110%'})
        await next({backgroundSize: '100%'})
      }
    },
    config: { duration: 15000 }
  })
  return (
    <>
      <animated.div style={PulseFx} className="HomeBGLeft"></animated.div>
      <animated.div style={PulseFx} className="HomeBGRight"></animated.div>
      <div className="d-flex align-items-center cover justify-content-center">
        <animated.div style={FadeIn} className="text-center IntroSection">
          <h4 className="font-weight-normal mb-0">Nantikan</h4>
          <h1 className="mt-0">2020</h1>
        </animated.div>
      </div>
    </>
  )
}