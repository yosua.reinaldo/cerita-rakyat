import React from 'react'
import {useSpring, animated} from 'react-spring'
import AssetsLogoDjarum from '../img/logo-djarum.png'

export default() => {
  const loading = 1;
  const FadeIn = useSpring({
    from: { opacity: 0, transform: `translate3d(0px, -20px, 0px)` },
    to : async next => {
      while(loading) {
        await next({opacity: 1, transform: `translate3d(0px, 0px, 0px)`})
        await next({ opacity: 1, transform: `translate3d(0px, -10px, 0px)`, config: { duration: 1500}})
      }
    },
    config: { duration: 1000 }
  })

  const PulseFx = useSpring({
    from: { backgroundSize: '100%'},
    to: async next => {
      while(loading) {
        await next({backgroundSize: '110%'})
        await next({backgroundSize: '100%'})
      }
    },
    config: { duration: 15000 }
  })
  return (
    <div className="App h-100 overflow-hidden" style={{overflow: 'none'}}>
      <animated.div style={PulseFx} className="HomeBGLeft"></animated.div>
      <animated.div style={PulseFx} className="HomeBGRight"></animated.div>
      <div className="position-fixed w-100 text-center p-4">
        <img src={AssetsLogoDjarum} alt="Djarum" style={{maxWidth: 200}} className="w-100" />
      </div>
      <div className="d-flex align-items-center cover justify-content-center">
        <animated.div style={FadeIn} className="text-center IntroSection">
          <h1 className="mt-0">Not Found</h1>
        </animated.div>
      </div>
    </div>
  )
}