import React from 'react'
import {useSpring, animated} from 'react-spring'
import './index.scss'

export default() => {
  const loading = 1;
  const LeftToRight = useSpring({
    from: { opacity: .5, transform: `scale(1)`, transform: `translate(0px, 0px)`},
    to: async next => {
      while(loading) {
        await next({opacity: 1, transform: `translate(30px, 20px)`})
        await next({opacity: 1, transform: `translate(0px, 0px)`})
      }
    },
    config: { duration: 10000 }
  })

  const RightToLeft = useSpring({
    from: { opacity: .2, transform: `scale(1)`, transform: `translate(0px, 0px)`},
    to: async next => {
      while(loading) {
        await next({opacity: 1, transform: `translate(-30px, 20px)`})
        await next({opacity: 1, transform: `translate(0px, 0px)`})
      }
    },
    config: { duration: 10000 }
  })
  return(
    <>
      <animated.div id="LeftTop" style={LeftToRight}></animated.div>
      <animated.div id="LeftBottom" style={LeftToRight}></animated.div>
      <animated.div id="RightTop" style={RightToLeft}></animated.div>
      <div id="Main"></div>
    </>
  )
}